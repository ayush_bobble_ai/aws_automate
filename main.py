import boto3
import datetime
from botocore.exceptions import ClientError
import subprocess
import time

allocation_id = 'eipalloc-0d4aa39ec60b6230a'

client = boto3.client('ec2')
host= '52.0.176.81'

while(True):
    ping_response = subprocess.call(['ping', '-c','4',host])
    if ping_response==0:
        print("Instance working!")
    else:
        spot_response = client.request_spot_instances(
            AvailabilityZoneGroup='us-east-1',
            DryRun=False,
            SpotPrice='0.1',
            ClientToken='string10',
            InstanceCount=1,
            Type='one-time',
            LaunchSpecification={
                'ImageId': 'ami-01615ce7a6f460151',
                'KeyName': 'mynewkeypair',
                'SecurityGroups': ['launch-wizard-1'],
                'InstanceType': 't2.micro',
                'Placement': {
                    'AvailabilityZone': 'us-east-1a',
                },
                'Monitoring': {
                    'Enabled': True
                },
                'SecurityGroupIds': [
                    'sg-09f1cc9e8724fe173',
                ]
            }
        )
        time.sleep(180)
        response = client.describe_spot_instance_requests(
            DryRun=False,
            SpotInstanceRequestIds=[
                spot_response['SpotInstanceRequests'][0]['SpotInstanceRequestId'],
            ]
        )
        print(response)
        instance_id = response['SpotInstanceRequests'][0]['InstanceId']
        print(instance_id)
        # if spot_response==0:
        #     print("Spot instance created successfully!!!")
        # else:
        #     print("error!")

        try:
            response = client.associate_address(AllocationId=allocation_id,
                                            InstanceId=instance_id)
            print(response)
        except ClientError as e:
            print(e)